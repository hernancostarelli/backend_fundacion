package com.fundacion.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "docente")
@Data
@EqualsAndHashCode(callSuper = true)
public class Docente extends Usuario {

	@NotEmpty(message = "EL NOMBRE DEL USUARIO NO PUEDE SER NULO O VACIO")
	@Size(min = 5, max = 15, message = "DEBE INGRESAR AL MENOS 5 CARACTERES")
	@Column(name = "username")
	private String username;

	@NotEmpty(message = "LA CONTRASEÑA DEL USUARIO NO PUEDE SER NULA O VACIA")
	@Size(min = 6, max = 15, message = "DEBE INGRESAR AL MENOS 6 CARACTERES")
	@Column(name = "password")
	private String password;
}
