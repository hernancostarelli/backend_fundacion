package com.fundacion.entidades;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity
@Table(name = "programa")
@Data
public class Programa {

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(unique = true, name = "id_programa")
	private String idPrograma;

	@NotEmpty(message = "EL NOMBRE NO PUEDE SER NULO") // a REVEER EN CRUD
	@Size(min = 6, max = 15, message = "DEBE INGRESAR AL MENOS 6 CARACTERES")
	@Column(name = "nombre")
	private String nombre;

	@NotEmpty(message = "LA DESCRIPCION NO PUEDE SER NULA") // a REVEER EN CRUD
	@Size(min = 6, max = 150, message = "DEBE INGRESAR AL MENOS 6 CARACTERES")
	@Column(name = "descripcion")
	private String descripcion;	

	@ManyToMany(mappedBy = "voluntariados", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Ayuda> voluntarios;
	
	@ManyToMany(mappedBy = "solicitudes", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Ayuda> solicitantes;
	
	// REVEER relacion con entidad Imagen	
	@OneToOne
	@JoinColumn(name = "portada")
	private Imagen portada;
	// REVEER relacion con entidad Imagen
	@OneToOne
	@JoinColumn(name = "img")
	private Imagen img;
	//private List<Imagen> imgenes;

}
