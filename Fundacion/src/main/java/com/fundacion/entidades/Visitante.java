package com.fundacion.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "visitante")
@Data
@EqualsAndHashCode(callSuper = true)
@Inheritance(strategy = InheritanceType.JOINED)
public class Visitante extends Usuario {

	@NotEmpty(message = "")//a REVEER EN CRUD
	@Size(min =6, max = 15, message = "DEBE INGRESAR AL MENOS 6 CARACTERES")
	@Column(name = "telefono")
	protected long telefono;

	@NotEmpty(message = "")//a REVEER EN CRUD
	@Size(min =6, max = 15, message = "DEBE INGRESAR AL MENOS 6 CARACTERES")
	@Column(name = "provincia")
	protected String provincia;

	@NotEmpty(message = "")//a REVEER EN CRUD
	@Size(min =6, max = 15, message = "DEBE INGRESAR AL MENOS 6 CARACTERES")
	@Column(name = "localidad")
	protected String localidad;

	@NotEmpty(message = "")//a REVEER EN CRUD
	@Size(min =6, max = 15, message = "DEBE INGRESAR AL MENOS 6 CARACTERES")
	@Column(name = "codigoPostal")
	protected String codigoPostal;

}
