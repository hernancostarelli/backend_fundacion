package com.fundacion.entidades;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fundacion.enumeraciones.ETipoAyuda;

import lombok.Data;
import lombok.EqualsAndHashCode;

//Ayuda brindada o solicitada
@Entity
@Table(name = "ayuda")
@Data
@EqualsAndHashCode(callSuper = true)
public class Ayuda extends Visitante {
	@ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinTable(name = "volunt_prog", joinColumns = @JoinColumn(name = "id_programa"), inverseJoinColumns = @JoinColumn(name = "id_usuario"))
	private List<Programa> voluntariados;

	@Column(name = "bio")
	private String bio;

	@ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinTable(name = "solic_prog", joinColumns = @JoinColumn(name = "id_programa"), inverseJoinColumns = @JoinColumn(name = "id_usuario"))
	private List<Programa> solicitudes;

	@Column(name = "tipo_ayuda")
	@Enumerated(EnumType.STRING)
	private ETipoAyuda tipoDeAyuda;
}
