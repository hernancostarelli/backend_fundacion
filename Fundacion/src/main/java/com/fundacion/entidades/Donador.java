package com.fundacion.entidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fundacion.enumeraciones.ETipoDonacion;
import com.fundacion.enumeraciones.ETipoDonante;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "donador")
@Data
@EqualsAndHashCode(callSuper = true)
public class Donador extends Visitante {

	//Agregar validacion en servicio o identidad = Mayor a 0, montos minimos.
	@Column(name = "monto")
	private Double monto;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha")
	private Date fecha = new Date();

	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_donacion")
	private ETipoDonacion tipoDonacion;

	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_donante")
	private ETipoDonante eTipoDonante;
}
