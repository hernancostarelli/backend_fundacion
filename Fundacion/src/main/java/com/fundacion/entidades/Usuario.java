package com.fundacion.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import com.fundacion.enumeraciones.ERol;

import lombok.Data;

@Inheritance(strategy = InheritanceType.JOINED)
@MappedSuperclass
@Data
public abstract class Usuario {

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(unique = true, name = "id_usuario")
	protected String idUsuario;

	@Email
	@NotEmpty(message = "LA DIRECCIÓN DE CORREO NO PUEDE ESTAR VACÍA")
	@Column(unique = true, name = "email")
	protected String email;

	@NotEmpty(message = "EL NOMBRE NO PUEDE ESTAR VACÍO")
	@Size(min = 3, max = 15, message = "DEBE INGRESAR AL MENOS 3 CARACTERES")
	@Column(name = "nombre", length = 15)
	protected String nombre;

	@NotEmpty(message = "EL APELLIDO NO PUEDE ESTAR VACÍO")
	@Size(min = 3, max = 15, message = "DEBE INGRESAR AL MENOS 3 CARACTERES")
	@Column(name = "apellido", length = 15)
	protected String apellido;

	@NotEmpty(message = "EL NÚMERO DE DOCUMENTO NO PUEDE ESTAR VACÍO")
	@Size(min = 8, max = 8, message = "LA LONGITUD DEL NÚMERO DE DOCUMENTO ES INCORRECTA")
	@Column(unique = true, name = "dni", length = 8)
	private String dni;

	@Column(name = "estado")
	protected Boolean estado = true;

	@Enumerated(EnumType.STRING)
	protected ERol rol;

}
