package com.fundacion.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity
@Data
public class Imagen {

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(unique = true, name = "id_imagen")
	private String idImagen;

	@NotEmpty(message = "LA DESCRIPCION NO PUEDE SER NULA") // a REVEER EN CRUD
	@Size(min = 6, max = 150, message = "DEBE INGRESAR AL MENOS 6 CARACTERES")
	@Column(name = "descripcion")
	private String descripcion;

	@NotEmpty(message = "EL PATH NO DEBE SER NULO") // a REVEER EN CRUD
	@Size(min = 6, max = 150, message = "DEBE INGRESAR AL MENOS 6 CARACTERES")
	@Column(name = "path")
	private String path;
}
